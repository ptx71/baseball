\documentclass{tufte-handout}

%\geometry{showframe}% for debugging purposes -- displays the margins

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{multirow,makecell}

% Set up the images/graphics package
\usepackage{graphicx}
\setkeys{Gin}{width=\linewidth,totalheight=\textheight,keepaspectratio}
\graphicspath{{graphics/}}

\title{Rebels 11U AA Practice Schedule}
\author[Coach Davis]{Coach Davis \& Coach Estrada}
\date{April 15, 2024}  % if the \date{} command is left out, the current date will be used

% The following package makes prettier tables.  We're all about the bling!
\usepackage{booktabs}
\usepackage{tikz}

% The units package provides nice, non-stacked fractions and better spacing
% for units.
\usepackage{units}

% The fancyvrb package lets us customize the formatting of verbatim
% environments.  We use a slightly smaller font.
\usepackage{fancyvrb}
\fvset{fontsize=\normalsize}

% Small sections of multiple columns
\usepackage{multicol}

% These commands are used to pretty-print LaTeX commands
\newcommand{\doccmd}[1]{\texttt{\textbackslash#1}}% command name -- adds backslash automatically
\newcommand{\docopt}[1]{\ensuremath{\langle}\textrm{\textit{#1}}\ensuremath{\rangle}}% optional command argument
\newcommand{\docarg}[1]{\textrm{\textit{#1}}}% (required) command argument
\newenvironment{docspec}{\begin{quote}\noindent}{\end{quote}}% command specification environment
\newcommand{\docenv}[1]{\textsf{#1}}% environment name
\newcommand{\docpkg}[1]{\texttt{#1}}% package name
\newcommand{\mybox}[2][black]{%
  \begin{tikzpicture}
    \node[rectangle,minimum width=8cm,minimum height=4cm, font=\Large, label=below:Coach] (A) {#2};
    \filldraw [black] (A.north west) circle (2pt);
    \filldraw [black] (A.north) circle (2pt);
    \filldraw [black] (A.north east) circle (2pt);
    \filldraw [black] (A.east) circle (2pt);
    \filldraw [black] (A.south west) circle (2pt);
    \filldraw [black] (A.south) circle (2pt);
    \filldraw [black] (A.south east) circle (2pt);
    \filldraw [black] (A.west) circle (2pt);
   \end{tikzpicture}%
}

\begin{document}

\maketitle% this prints the handout title, author, and date

\begin{marginfigure}%
  \includegraphics[width=\linewidth]{rebels-logo}
\end{marginfigure}

\section{Practice Goals}
\newthought{Getting our energy level back up.}  The team has improved dramatically in the last two months.
We need to convince the kids how much better they are!  Music and energy are on the menu to get them jacked up for the week ahead.

\begin{table}
\begin{tabular}{|c||c|}
  \hline
  \multicolumn{2}{|c|}{Monday, April 15 2024 Practice Schedule} \\
  \hline
  \hline
  Time Range & Drill \\
  \hline
  4:30–4:40 & Body Warmup \\
  4:40–4:50 & Baseball Warmup \\
  4:50–7:30 & Signals and baserunning situations \\
  4:50–5:30 & \makecell{BP: Heavy Balls \\ 8-Ball \\ Infield} \\
  5:30–6:00 & \makecell{Bunting \\ Fielding a Bunt} \\ 
  6:00–6:05 & Team Meeting \\
  \hline
\end{tabular}
\end{table}

\newpage

\section{Warmup Routine}
\newthought{Body Warmups:} Coach Estrada.

\newthought{Game Throws + Movements} with Four Corners.

\section{Drills + Coaching Points}
\newthought{Four Corners} is a drill to warm up baseball movement, throwing, and tempo.  We will do this before every practice!  The drill is as follows:
\begin{itemize}
  \item Groups of 4, each player needs their glove and one baseball.
  \item Each group decides who is "home."  The rest of the positions flow naturally.
  \item Form a rectangle, each side approximately 30 feet.
  \item Whoever is "home" dictates the speed of this drill.
  \item Begin by throwing catcher$\longrightarrow$first base...third base$\longrightarrow$catcher
  \item Start with measured throws; go around the horn once.
  \item After going around the horn once, reverse the ball flow, now we are catcher$\longrightarrow$third base...first base$\longrightarrow$catcher
  \item Change directions again (catcher$\longrightarrow$first base...third base$\longrightarrow$catcher) with increased tempo
  \item Repeat the direction change
  \item Increase tempo one more time
  \item The goal is six completed cycles around the horn. 
  \item Each group must start over if a ball is dropped.
\end{itemize}

\newpage

\section{Drills}

\newthought{8-Ball}\sidenote{Coaching Points:
\begin{itemize}
  \item{Getting a player comfortable with running to a position to wait on the catch.}
  \item{Getting a player comfortable with flipping their hips to run backward.}
  \item{Catching with their face.}
\end{itemize}}
is an outfield drill designed to teach catching on the move.  The drill is as follows:

\begin{itemize}
	\item Create a rectangle roughly 30 yards in width and 20 yards in height (see diagram below).
  \item Place 8 cones at the corners and midpoints of the rectangle (see diagram below).
  \item Each player starts at the center of the rectangle (see diagram below).
  \item With the coach at the bottom of the rectangle, throw one pop-up near where the player currently stands to warm them up.
  \item With the coach at the bottom of the rectangle, throw three \textit{high pop-ups} to random cones for each player.
	  The goal is to have the player \textit{sprint into position and then await the catch}.
  \item With the coach at the bottom of the rectangle, throw three \textit{low flyballs} to random cones for each player.
	  The goal is to have each player \textit{catch the ball on the run or near run}.
  \item After 7 reps, rotate each player.
\end{itemize}

\vspace{1em}

\mybox{Player}

\vspace{2em}

\newthought{BP: Heavy Balls.}  Drill is as follows:

\begin{itemize}
  \item No helmets are required for this.
  \item Kids take takes throwing soft BP in the outfield
  \item 10 solid pitches, rotate
\end{itemize}

\newthought{Infield (in the outfield if we are sharing the field).} Drill is as follows:

\begin{itemize}
  \item Field a ground ball.
  \item \textit{Make a baseball play} by executing the throw after fielding.
\end{itemize}

\newthought{Bunting\sidenote{Coaching Points:
\begin{itemize}
	\item{Player squares up to the pitcher when their windup begins.}
  \item{Player places their top hand near the base of the barrel, pinching the barrel like "like a snapping turtle". All knuckles must be behind the bat!}
  \item{The barrel of the bat must be "up" to avoid popping the ball into the air.}
  \item{The player raises/lowers their entire body to get on plane with the ball.}
  \item{The player "catches" the ball with the bat to shorten the roll.}
\end{itemize}}
and Fielding a Bunt\sidenote{Coaching Points:
\begin{itemize}
	\item When there is no runner on second, the third baseman \textit{sprints towards homeplate when the batter squares up.} 
	\item The first baseman \textit{never} chases the ball. His job is back at first!
	\item The pitcher sprints forward off the mound, aiming between first base and the pitchers mound. The third-baseman has the third base side.
        \item If the ball is stopped, pick it up with your bare hand. If it is not, with your glove.
	\item \textit{ALWAYS TAKE A PAUSE BEFORE FIRING THE THROW TO FIRST}.
\end{itemize}}.}  Drill is as follows:

\begin{itemize}
  \item  Blue team takes the field
  \item  Coach will "roll" the ball to the players a few times to practice fielding the bunt.
  \item  Everyone not on the field lines up to bunt
  \item  Each batter bunts three times 
  \item  On the fourth bunt, it is in play and the fielders must make the play
  \item  Pitchers rotate after each hitter
  \item  After one cycle, each player goes again. This time, the first bunt is in play.
  \item  Red team takes the field, everyone else lines up to bunt. There will be repeaters.
\end{itemize}

\begin{table}
\begin{tabular}{|c||c|}
  \hline
  \multicolumn{2}{|c|}{Blue Team} \\
  \hline
	Position & Player(s)  \\
  \hline
	First Base & Karma \\
	Third Base & DJ \\
	Catcher & Adrian \\
	Pitchers & Grady / Jyles \\
  \hline
\end{tabular}
\end{table}

\begin{table}
\begin{tabular}{|c||c|}
  \hline
  \multicolumn{2}{|c|}{Red Team} \\
  \hline
	Position & Player(s)  \\
  \hline
	First Base & Grady \\
	Third Base & Connor \\
	Catcher & Daniel \\
	Pitchers & Logan / Karma \\
  \hline
\end{tabular}
\end{table}


\end{document}
