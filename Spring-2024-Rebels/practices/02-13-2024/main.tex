\documentclass{tufte-handout}

%\geometry{showframe}% for debugging purposes -- displays the margins

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{multirow,makecell}

% Set up the images/graphics package
\usepackage{graphicx}
\setkeys{Gin}{width=\linewidth,totalheight=\textheight,keepaspectratio}
\graphicspath{{graphics/}}

\title{Rebels 11U AA Practice Schedule}
\author[Coach Davis]{Coach Davis \& Coach Estrada}
\date{February 13, 2024}  % if the \date{} command is left out, the current date will be used

% The following package makes prettier tables.  We're all about the bling!
\usepackage{booktabs}

% The units package provides nice, non-stacked fractions and better spacing
% for units.
\usepackage{units}

% The fancyvrb package lets us customize the formatting of verbatim
% environments.  We use a slightly smaller font.
\usepackage{fancyvrb}
\fvset{fontsize=\normalsize}

% Small sections of multiple columns
\usepackage{multicol}

% These commands are used to pretty-print LaTeX commands
\newcommand{\doccmd}[1]{\texttt{\textbackslash#1}}% command name -- adds backslash automatically
\newcommand{\docopt}[1]{\ensuremath{\langle}\textrm{\textit{#1}}\ensuremath{\rangle}}% optional command argument
\newcommand{\docarg}[1]{\textrm{\textit{#1}}}% (required) command argument
\newenvironment{docspec}{\begin{quote}\noindent}{\end{quote}}% command specification environment
\newcommand{\docenv}[1]{\textsf{#1}}% environment name
\newcommand{\docpkg}[1]{\texttt{#1}}% package name

\begin{document}

\maketitle% this prints the handout title, author, and date

\begin{marginfigure}%
  \includegraphics[width=\linewidth]{rebels-logo}
\end{marginfigure}

\section{Practice Goals}
\newthought{Establishing routine.}  This practice will be slightly slower paced than subsequent practices, as we are implementing five new drills today.  The goal is for each of you to leave today understanding the following:

\begin{itemize}
  \item The three components of our warmup routine.
  \item Ninety focused and disciplined minutes $\ggg$ nine unfocused hours
  \item What are the expectations for each of you as a member of the Rebels?
  \item Gain an understanding of how practices will typically flow.
  \item Have fun!
\end{itemize}

\section{Schedule (for our first practice, a best attempt)}
\begin{table}
\begin{tabular}{|c||c|}
  \hline
  \multicolumn{2}{|c|}{Tuesday, February 13 2023 Practice Schedule} \\
  \hline
  \hline
  Time Range & Drill \\
  \hline
  6:00–6:05 & Introductions \\
  6:05–6:20 & Warmup Routine \\
  6:20–6:30 & First Drill Set: Explanations \\
  6:30–7:00 & \makecell{BP: Heavy Balls \\ Bombadier \\ Sekiro} \\
  7:00–7:10 & Second Drill Set: Explanations \\
  7:10–7:30 & \makecell{Beat the Ball \\ Two-Ball} \\ 
  7:30–7:35 & Cooldown \\
  7:35–7:45 & Team Meeting \\
  \hline
\end{tabular}
\end{table}

\section{Warmup Routine}
\newthought{Warmups will be the same} for every practice.  There are three components to each warmup.

\newthought{Stretching} is the first thing we do before throwing. 
\begin{itemize}
  \item Knee to Chest
  \item 10 bodyweight squats
  \item High Knees
  \item Short Sprint
  \item Arm Circles
  \item Tricep Stretch
  \item Half-Pole
\end{itemize}

\newthought{Throwing}
\begin{itemize}
	\item Flipping Ball Drill\sidenote{Coaching Points:\begin{itemize}\item{Need to feel the Ball coming off the tips of their fingers (middle finger primarily) as they throw the Ball.}\item{Top fingers finish under the Ball to provide the backward spin.}\item{No sideways spin}\end{itemize}}, Twelve Rotations
			\item Throwing from Knees\sidenote{Coaching Points:\begin{itemize}\item{Stay balanced with weight centered}\item{Separation of arms with throwing arm coming down and around.}\item{Accurate throws.}\end{itemize}}, Twelve Rotations
  \item Regular Throws, Twelve Rotations 
\end{itemize}

\newthought{Game Throws + Movements} with Four Corners.

\section{Drills + Coaching Points}
\newthought{Four Corners} is a drill to warm up baseball movement, throwing, and tempo.  We will do this before every practice!  The drill is as follows:
\begin{itemize}
  \item Groups of 4, each player needs their glove and one baseball.
  \item Each group decides who is "home."  The rest of the positions flow naturally.
  \item Form a rectangle, each side approximately 30 feet.
  \item Whoever is "home" dictates the speed of this drill.
  \item Begin by throwing catcher$\longrightarrow$first base...third base$\longrightarrow$catcher
  \item Start with measured throws; go around the horn once.
  \item After going around the horn once, reverse the ball flow, now we are catcher$\longrightarrow$third base...first base$\longrightarrow$catcher
  \item Change directions again (catcher$\longrightarrow$first base...third base$\longrightarrow$catcher) with increased tempo
  \item Repeat the direction change
  \item Increase tempo one more time
  \item The goal is six completed cycles around the horn. 
  \item Each group must start over if a ball is dropped.
\end{itemize}

\newthought{Bombadier}\sidenote{Coaching Points:\begin{itemize}\item{Laughter!}\item{Getting a player comfortable catching with their face.}\end{itemize}} is an outfield drill designed to teach body positioning on the catch.  The drill is as follows:

\begin{itemize}
  \item Each kid needs a helmet for this drill.
  \item Using a tennis racket, the coach hits various flyballs (some easier, some higher)
  \item Each kid should position themselves under the Ball and let it hit them in the head
  \item Each kid collects the tennis ball near themselves; when the coach supply is exhausted, the balls are transferred
  \item Rotate each kid for 7–10 minutes
\end{itemize}

\newthought{Sekiro}\sidenote{Coaching Points:\begin{itemize}\item{Eyes on the pitcher's hat until he goes into motion}\item{Eyes move to the box where the Ball is released}\item{Eyes track the flight of the Ball to the glove}\item{The player CANNOT step away from the plate}\end{itemize}} is a hitting+pitching drill designed to get kids comfortable with the Ball coming towards them and to train their eyes.  The drill is as follows:

\begin{itemize}
  \item Each rotation needs one pitcher (the coach can pitch to the pitcher to ensure he gets reps)
  \item Each player needs a helmet.
  \item Each player walks up to the plate, positioning themselves in the batter's box as if they were hitting.  They are wearing an opposite-hand glove.
  \item Using their eye plan, each player makes a hitting motion.  However, the goal is to catch (or knockdown) the Ball. 
  \item To start, do five pitches and rotate.  Later, we will do regular at-bats.
  \item The coach is the catcher in this situation, as no bat is utilized.
\end{itemize}

\newthought{BP: Heavy Balls.}  Drill is as follows:

\begin{itemize}
  \item No helmets are required for this.
  \item Kids take takes throwing soft BP in the outfield
  \item 10 solid pitches, rotate
\end{itemize}

\newthought{Two Ball}\sidenote{Coaching Points:\begin{itemize}\item{Force each kid to catch a ball with each hand.  Do not let them catch with their arms.}\item{For kids that are comfortable, tell them to keep their eyes locked on you while trying to catch.} \end{itemize}} is a drill designed to increase hand-eye coordination.  The drill is as follows:

\begin{itemize}
  \item The first time, a coach or adult stands in the middle of a circle of kids with a radius of 5–7 yards.
  \item The coach locks eyes with a player, then soft tosses two balls (one from each hand) to the player.
  \item The player must catch both balls (one in each hand). 
  \item The player soft-tosses the two balls back to the coach.
\end{itemize}

\newthought{Beat the Ball}\sidenote{Coaching Points:\begin{itemize}\item{This is all about fielding; just make the runner go fast.}\item{The runner is to increase the mental clock of the players and simulate game situations; this drill must be done with tempo}\item{If the kids can execute the drill well, gamify by seeing how they can complete the diamond the quickest}\end{itemize}} is a drill designed to simulate game movements for infielders and to force them to make accurate throws with tempo and correct foot movement.  The drill is as follows:  

\begin{itemize}
  \item Six players minimum for this drill; each player needs their gloves.
  \item Start with four kids at each base and two players ready to run.
  \item Place a baseball on the ground between \(\frac{1}{3}\) and \(\frac{1}{2}\) of the way to third base.
  \item The runner starts at home plate
  \item On "GO," the runner takes off for first and the third baseman charges the Ball and fields it as if it were a live baseball play.
  \item The runner ignores the Ball and runs as hard as they can around the bases for a home run, no matter what.
  \item The first baseman catches the Ball as if they were trying to make the out.  He then pivots and throws to the second baseman all the way around the horn.
  \item The player MUST be touching the bag for a caught ball to count.
  \item The goal is to get the player out at each base.
  \item After each turn, rotate players. catcher$\longrightarrow$first base...third base$\longrightarrow$runner
\end{itemize}

\section{Cooldowns}
\newthought{We cool down to avoid injuries}
\begin{itemize}
  \item One full pole
  \item Tricep stretches for arm
\end{itemize}

\end{document}

